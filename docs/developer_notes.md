# Windows Exploit Development Plugin

## Important Notes
- Dont use DbgEng interfaces in any thread outside of the main thread, it will cuase a deadlock
- To execute a windbg command and get output we need to register an IDebugOutputClient, call the command wait for the event and get the data from the callback.  (https://githomelab.ru/kdlibcpp/kdlibcpp/blob/dev-1.0/kdlib/source/win/dbgeng.cpp) a semi example here.

## Resources
- https://github.com/anhkgg/awesome-windbg-extensions
- https://github.com/0cch/0cchext/blob/master/0cchext/0cchext.cpp

## WinDbg Native Tricks
- Search all executable memory regions for a hex pattern
  - `!address /f:PAGE_EXECUTE,PAGE_EXECUTE_READ,PAGE_EXECUTE_READWRITE,PAGE_EXECUTE /c:"s %1 %2 54 c3"`
- Memory Protections and Mappings
  - !address
  - !address ADDRESS
  - !vprot ADDRESS
