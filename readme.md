# Windows Exploit Development Plugin (wedp)
The Windows Exploit Development Plugin, knowd as wedp, is a native windbg extenstion that has functionality to help with exploit development.
We originally started development on this because we wanted something that was easier to install and faster than the old standard mona.py.
As well, we wanted to natively support both x86 and x64 and with WinDbg being the target debugger, there is potential for future work in supporting kernel exploit dev.
We also do not intend to add all of the different options that mona.py supports, so be prepared to build your own rop chains, but dont fret you only have to wait seconds rather than minutes for all rop gadgets to be found.

## Usage
**NOTE**: The code blocks showing different screens of WEDP may not be kept up to date, we will try to update them when there are major changes.

### Loading
wedp is build and released as a WinDbg extension DLL that must be loaded into windbg (or other debuggers built on the Windows Debugging Engine).
There are 2 options for loading the extension using the `.load` command.

1. Place the extension DLL in the `winext` directory for the proper platform where the debuggers are installed. The default location for this is `C:\Program Files (x86)\Windows Kits\10\Debuggers\x64\winext` or `C:\Program Files (x86)\Windows Kits\10\Debuggers\x86\winext` for the x86 build of the debugger. For this method you would them simply pass the module name `wedp` to the `.load` command to load it.

```
0:000> .load wedp
__          ________ _____  _____      
\ \        / /  ____|  __ \|  __  \    
 \ \  /\  / /| |__  | |  | | |__) |    
  \ \/  \/ / |  __| | |  | |  ___/     
   \  /\  /  | |____| |__| | |         
    \/  \/   |______|_____/|_|         
          VERSION: 0.0.1
```

2. The next method would be to use an absolute path to the module (including .dll) in the command
```
0:000> .load C:\Users\user\wedp\wedp.dll
__          ________ _____  _____      
\ \        / /  ____|  __ \|  __  \    
 \ \  /\  / /| |__  | |  | | |__) |    
  \ \/  \/ / |  __| | |  | |  ___/     
   \  /\  /  | |____| |__| | |         
    \/  \/   |______|_____/|_|         
          VERSION: 0.0.1

```

### Executing Commands
Once wedp is loaded you can see the entire list of available commands by calling `!wedp.help`

```
0:000> !wedp.help
Commands for C:\Users\user\wedp\wedp.dll:
  !help             - Displays information on available extension commands
  !wedp_asm         - Assemble intel format assembly instructions into raw
                      bytes
  !wedp_disasm      - Disassemble raw bytes into intel formatted assembly
                      instructions
  !wedp_getopts     - Output the current global options for the session
  !wedp_iat         - Parses and outputs information from the Import Address
                      Table. This includes the address of a loaded function as
                      well as the IAT address where that function pointer
                      resides. By default this will get all gadgets from
                      executeable sections of every loaded module.
  !wedp_memory      - Outputs information about memory usage in the address
                      space. This includes information such as the type of
                      memory and the protections on the memory
  !wedp_modules     - Outputs all of the modules loaded in the current debugee
                      process and detailed information about that module. This
                      includes information useful to exploitation such as the
                      enabled/disabled protections (ASLR, DEP, SAFESEH, etc.).
  !wedp_offset      - Calculates and outputs an offset between two addresses
  !wedp_pattern_gen - Generate an alphanumeric pattern that is used to find
                      offsets during exploitation (MAX LEN: 0n20280 or 0x4f38)
  !wedp_pattern_off - Find the offset of an input chunk from the alphanumeric
                      pattern in the format generated by !wedp_pattern_gen
  !wedp_redirect    - Find jmp and call instructions that are useful to
                      redirect execution. By default this will get all gadgets
                      from executeable sections of every loaded module.
  !wedp_rop         - Outputs ROP gadgets. By default this will get all gadgets
                      from executeable sections of every loaded module.
  !wedp_seh         - Outputs SEH gadgets. By default this will get all gadgets
                      from executeable sections of every loaded module.
  !wedp_setopts     - Set global options for the session
  !wedp_stackpivot  - Find stackpivot instructions. By default this will get
                      all gadgets from executeable sections of every loaded
                      module.
  !wedp_version     - Outputs the Windows Exploit Development Plugin (wedp)
```

For help with a specific command you can use the `!help` command followed by the command to get more detailed help. See the example below:

```
0:000> !help wedp_rop
!wedp_rop [/c <num>] [/o <file>] [/mi <modules>] [/me <modules>]
          [/mc <characteristics>] [/ap <protections>] [/ab <bad addresses>]
          [/ac <characteristics>]
  /c <num> -  A debug engine numerical expression for the number of results to
             output (ex. 0n100 or 0x100) (space-delimited)
  /o <file> -  Abosolute path or filename to write current commands output to,
              if a filename only is provided the default path or path set with
              /w is used
  /mi <modules> -  Comma seperated list with no spaces of modules to INCLUDE in
                  the search (ex. kernel32,ntdll)
  /me <modules> -  Comma seperated list with no spaces of modules to EXCLUDE
                  from the search (ex. kernel32,ntdll)
  /mc <characteristics> -  Comma seperated list of charactertics a module must
                          match to be included in a search. The format is
                          <characteristic>=[t|f|*] and the following
                          characteristics are available:  aslr, dep, safeseh,
                          cfg, os
  /ap <protections> -  Address Protections - Comma seperated list of memory
                      protections that the memory at a returned address must
                      meet. The format for these are: r, rw, rwx
  /ab <bad addresses> -  Address Bad Characters - A string of \x## formatted
                        characters that may not be present in an address
  /ac <characteristics> -  Address Characteristics - A comma seperated list of
                          characteristics an address should have
Outputs ROP gadgets. By default this will get all gadgets from executeable
sections of every loaded module.
```

wedp has a few global options that can be set with the `!wedp_setopts` command.
These global options will apply to all commands that are executed after the options is set.
You can see your currently set options with `!wedp_getopts`.

### Workflow
A big reason I started to work on wedp is that I wanted to have as much of my workflow exist in WinDbg as possible for writing an exploit.
The following is the general flow that I use when writing an exploit with wedp.

1. Open the executable in WinDbg, load wedp and executing the following commands to get some basic information: `!wedp_modules`, `!wedp_memory`. These two commands will give me a good understanding of if I have any modules and/or memory with weak protections that will be valuable to work with later in the process. If I find that DEP/ASLR exists in every module, I know I will need to be on the lookout for leaks or other issues that may help bypass ASLR when building out a ROP chain.
2. Once I find a crash, I will use `!wedp_pattern_gen` to generate a pattern and then use that as my overflow string. I then crash the program and see if I control any registers, memory locations that are important, data in objects, etc. At this point I can start getting known offsets. I may use this pattern over a few iterations. An example is, I may need to use the pattern in a memory leak to find an offset to a stack canary, then in the next iteration we can use the pattern for the RIP offset and offsets in registers.
3. The next step is EIP/RIP control. If we are on something simple and dont need a ROP chain, we can use `!wedp_redirects` to find a nice gadget to redirect execution to our shellcode. If its not, then its time to start building out our ROP chain. For this I like to do `!wedp_rop` and `!wedp_iat` and output those both to a file. My general workflow to build the rop chain from those is to use the text file in vs code to search through the gadgets to find what I need. It is nice to use the `/mi` filter here to only get output for modules we want (i.e. modules we can leak an address for or modules without ASLR).
4. Throughout the process I may use `!wedp_asm` and `!wedp_disasm` to figure out some certain bytes I may be looking for. A good example is if im looking for a debug break ROP gadget, I may disasm 0xcc becuase I forgot what the instruction was... but that only happens when I dont do exploitation for a while.



## Build
If you just want to use wedp, see the [releases](https://gitlab.com/ntninja-dev/windows-exploit-development/wedp/-/releases) page.
Otherwise, with a visual studio 2022 installation you can run the following commands from a developers command prompt.

### Release Build
```
git clone --recurse-submodules git@gitlab.com:ntninja-dev/windows-exploit-development/wedp.git
cd wedp\
msbuild "wedp\\wedp.sln" /p:Configuration=Release /p:Platform="x64"
msbuild "wedp\\wedp.sln" /p:Configuration=Release /p:Platform="x86"
```

### Debug Build
```
git clone --recurse-submodules git@gitlab.com:ntninja-dev/windows-exploit-development/wedp.git
cd wedp\
msbuild "wedp\\wedp.sln" /p:Configuration=Debug /p:Platform="x64"
msbuild "wedp\\wedp.sln" /p:Configuration=Debug /p:Platform="x86"
```

### Additional Build Details
wedp is distributed as a Visual Studio solution using the Visual Studio 2022 (v143) platform toolset and targeted at the latest 10.0 SDK.
wedp depends on several other projects, listed in the [dependencies](readme.md#depednecies) section, which are all included as submodules.
The project executes a pre-link event to build all of the dependencies as static libraries and links them into the final wedp dll.

### Depednecies
- [asmjit](https://github.com/asmjit/asmjit)
- [asmtk](https://github.com/asmjit/asmtk)
- [zydis](https://github.com/zyantific/zydis)
- [libfort](https://github.com/seleznevae/libfort)

## Developers
- ntninjadev (Ryan Johnson)