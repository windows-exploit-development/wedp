// asmtk breaks if it is included after windows.h as
// it requires WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#include <asmtk/asmtk.h>
#include <asmjit/x86.h>
#include "wedp.h"
#include "wedp_asm.h"

extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

	std::vector<BYTE> WedpAsm::assemble(std::string instructions)
	{
		BYTE* output = NULL;
		std::vector<BYTE> vec = std::vector<BYTE>();
		// asmjit::CodeInfo codeInfo;
		asmjit::Environment codeEnv;
		asmjit::CodeHolder codeHolder;

        // Determine whether we need to assemble x86 or x64 mnemonics based on the current
        // effective processor of the debugger
		codeEnv = asmjit::Environment(asmjit::Arch::kX64);
		if (IMAGE_FILE_MACHINE_I386 == EXT_INSTANCE.GetEffectiveProcessor()) {
			codeEnv = asmjit::Environment(asmjit::Arch::kX86);
		}
		
        // Intialize asmtk and asmjit so we can parse and assemble the input
		codeHolder.init(codeEnv);
		asmjit::x86::Assembler assembler(&codeHolder);
		asmtk::AsmParser parser(&assembler);

		// Parse the input assembly instructions
        // asmjit::Error provides an error string that we can use for output later
		asmjit::Error err = parser.parse(instructions.c_str());
		if (err) {
			goto cleanup;
		}

		// The opcodes will all be in .text, which is always section 0
		asmjit::CodeBuffer& buffer = codeHolder.sectionById(0)->buffer();

        // Cas the code buffer to a BYTE array then convert to a vector
		output = reinterpret_cast<BYTE*>(buffer.data());
		vec = std::vector<BYTE>(output, output + buffer.size());
	
	cleanup:
		return vec;
	}

   
    std::vector<BYTE> WedpAsm::assemble_delimited(std::string instructions, const char delim)
    {
        std::replace(instructions.begin(), instructions.end(), delim, '\n');
        return this->assemble(instructions);
    }

} // namespace wedp