#pragma once
#include <Zydis/Zydis.h>
#include <Windows.h>
#include <string>
#include <list>
#include <vector>
#include <set>


namespace wedp {
    #define INST_BUFFER_LEN 64
    #define MAX_USEFUL_INST_LEN ZYDIS_MAX_INSTRUCTION_LENGTH
    
    class WedpGadgetTree;

    // WedpAsm contains methods that handle assembling instructions
    class WedpDisasm {

        public:
            WedpDisasm();

            // Disassemble the instruction at offset and return a WedpGadgetTree* that may be used as a potential root
            WedpGadgetTree* disasm_instruction(std::vector<BYTE>& bytes, ULONG offset, WORD machine);

            // Build a tree of gadgets backwards from the input root, to the specified depth of instructions
            bool disasm_backwards(std::vector<BYTE>& bytes, ULONG depth, WedpGadgetTree *root);

            // Format an instruction into a std::string
            std::string disasm_format(WedpGadgetTree* inst);

            // Get the count of explicit operands for an instruction
            ULONG disasm_operand_count(WedpGadgetTree* curr);

            // Disassemble some instructions
            void disassemble(std::string);

        private:
            // Check for bad instructions that would break a gadget
            bool disasm_short_circuit(ZydisDisassembledInstruction inst);

            //ZydisDecoder x86decoder;
            //ZydisDecoder x64decoder;
            //ZydisFormatter formatter;
    };

} // namespace wedp