#pragma warning(disable: 4127)
#include <fort.hpp>
#pragma warning(default: 4127)

#include "wedp.h"
#include "wedp_gadgets.h"


#include <algorithm>
#include <sstream>
#include <iomanip>

extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

    WedpGadgetTree::~WedpGadgetTree()
    {
        for (WedpGadgetTree* curr : this->children) {
            delete curr;
        }
    }

    WedpGadgetSearch::WedpGadgetSearch(const WEDP_MEMORY *mem, const WEDP_MODULE* mod) {
        this->memory = mem;
        this->module = mod;
        // this->gadgets = new std::list<WEDP_GADGET>();
        this->search_complete = CreateEventA(NULL, TRUE, FALSE, NULL);
        this->search_thread = NULL;
    }


    WedpGadgetSearch::~WedpGadgetSearch()
    {
        if (this->search_complete) {
            CloseHandle(this->search_complete);
        }
        if (this->search_thread) {
            CloseHandle(this->search_thread);
        }
        
        // delete this->gadgets;
    }

    
    void WedpGadgetSearch::start_search()
    {
        this->search_thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)WedpGadgetSearch::run_search, (LPVOID)this, 0, NULL);  
        return;
    }
    
    
    bool WedpGadgetSearch::wait_completion()
    {
        DWORD dwRet = 0;
        bool ret = false;

        dwRet = WaitForSingleObject(this->search_thread, INFINITE);
        if (WAIT_OBJECT_0 != dwRet) {
            ret = false;
            goto cleanup;
        }
        
        ret = true;

    cleanup:
        SetEvent(this->search_complete);
        CloseHandle(this->search_thread);
        this->search_thread = NULL;
        return ret;
    }


    void WINAPI WedpGadgetSearch::run_search(LPVOID thisptr)
    {
        // Should search for all gadget type in a single pass over a memory region
        WedpGadgetSearch *currsearch = (WedpGadgetSearch*)thisptr;
        WedpGadgetTree* root = nullptr;
        std::vector<BYTE> *debuggemem = new std::vector<BYTE>(currsearch->module->size);
        SIZE_T read = 0;

        if (!ReadProcessMemory((HANDLE)EXT_INSTANCE.hProcess, (LPCVOID)currsearch->memory->base, debuggemem->data(), (SIZE_T)currsearch->memory->size, &read) && read != 0) {
            goto cleanup;
        }

        for (ULONG i = 0; i < read && i < currsearch->module->size; i++) {            

            // Try to disassemble the instruction at the current offset
            root = EXT_INSTANCE.h_Disasm.disasm_instruction(*debuggemem, i, currsearch->module->arch);
            if (nullptr == root) {
                continue;
            }

            // Based on the type of instruction we disasm backwards a certain depth and build out
            // the gadget tree from root
            switch (root->instruction.info.mnemonic) {
                case ZydisMnemonic::ZYDIS_MNEMONIC_RET:
                    
                    // Filter out BND enabled rets
                    if (root->instruction.info.attributes & ZYDIS_ATTRIB_HAS_BND) {
                        goto loop_cleanup;
                    }

                    // Filter our far rets completely currently
                    switch (root->instruction.info.opcode) {
                        case 0xc3:
                            EXT_INSTANCE.h_Disasm.disasm_backwards(*debuggemem, WEDP_ROP_DEPTH, root);
                            break;
                        case 0xc2:
                            EXT_INSTANCE.h_Disasm.disasm_backwards(*debuggemem, WEDP_ROP_OFF_DEPTH, root);
                            break;
                        default:
                            goto loop_cleanup;
                    }
                    break;
                case ZydisMnemonic::ZYDIS_MNEMONIC_JMP:
                    EXT_INSTANCE.h_Disasm.disasm_backwards(*debuggemem, WEDP_JMP_DEPTH, root);
                    break;
                case ZydisMnemonic::ZYDIS_MNEMONIC_CALL:
                    EXT_INSTANCE.h_Disasm.disasm_backwards(*debuggemem, WEDP_CALL_DEPTH, root);
                    break;
                default:
                    goto loop_cleanup;
            }

            // Classify the gadgets and build that gadget structure for them
            currsearch->parse_gadget_tree(root);

        loop_cleanup:
            if (nullptr != root) {
                delete root;
                root = nullptr;
            }
        }

    cleanup:
        delete debuggemem;
        ExitThread(0);
    }

    void WedpGadgetSearch::parse_gadget_tree(WedpGadgetTree* root)
    {
        std::queue<WedpGadgetTree*> bfsGadgetQueue;
        ULONG baseType = 0;

        switch (root->instruction.info.mnemonic) {
            case ZydisMnemonic::ZYDIS_MNEMONIC_RET:
                // Just set the base type, ret itself is not useful to store
                switch (root->instruction.info.opcode) {
                    case 0xc3:
                        baseType = GADGET_ROP;
                        break;
                    case 0xc2:
                        baseType = GADGET_ROP_OFF;
                        break;
                    default:
                        baseType = GADGET_INVALID;
                }
                break;
            case ZydisMnemonic::ZYDIS_MNEMONIC_JMP:
            case ZydisMnemonic::ZYDIS_MNEMONIC_CALL:
                baseType = GADGET_REDIRECT;
                if (root->instruction.info.operand_count &&
                    ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == root->instruction.operands[0].type)
                {
                    this->insert_gadget(root, baseType);
                }
                break;
        }

        // Push all the root children into the search queue to get a breadth
        // first search of gadgets
        for (WedpGadgetTree* child : root->children) {
            bfsGadgetQueue.push(child);
        }

        while (!bfsGadgetQueue.empty()) {
            WedpGadgetTree* curr = bfsGadgetQueue.front();
            bfsGadgetQueue.pop();

            for (WedpGadgetTree* child : curr->children) {
                bfsGadgetQueue.push(child);
            }

            ULONG type = this->classify_gadget(curr, baseType);
            if (type) {
                this->insert_gadget(curr, type);
            }
        }

        return;
    }


    //- jmp reg
    //- call reg
    //- push reg + ret(+offsets)
    //- push reg + pop r32 + jmp r32
    //- push reg + pop r32 + call r32
    //push reg + pop r32 + push r32 + ret(+offset)
    //- xchg reg, r32 + jmp r32
    //- xchg reg, r32 + call r32
    //xchg reg, r32 + push r32 + ret(+offset)
    //- xchg r32, reg + jmp r32
    //- xchg r32, reg + call r32
    //xchg r32, reg + push r32 + ret(+offset)
    //- mov r32, reg + jmp r32
    //- mov r32, reg + call r32
    //mov r32, reg + push r32 + ret(+offset)
    ULONG WedpGadgetSearch::classify_gadget(WedpGadgetTree* curr, ULONG baseType)
    {
        WedpGadgetTree* local = curr;
        ULONG type = GADGET_INVALID;

        switch (baseType) {
            case GADGET_ROP:
            case GADGET_ROP_OFF:
            {
                if (GADGET_ROP & baseType) {
                    type = baseType;
                }

                if (1 == curr->treeDepth) {

                    // Check for push reg; ret
                    if (ZydisMnemonic::ZYDIS_MNEMONIC_PUSH == curr->instruction.info.mnemonic &&
                        ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == curr->instruction.operands[0].type)
                    {
                        type |= GADGET_REDIRECT;
                    }

                    // Check for {add | sub} esp, {reg, imm, mem}; ret
                    if ((ZydisMnemonic::ZYDIS_MNEMONIC_ADD == curr->instruction.info.mnemonic ||
                        ZydisMnemonic::ZYDIS_MNEMONIC_SUB == curr->instruction.info.mnemonic) &&
                        ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == curr->instruction.operands[0].type &&
                        ZydisRegister::ZYDIS_REGISTER_ESP == curr->instruction.operands[0].reg.value)
                    {
                        type |= GADGET_STACKPIVOT;
                    }
                }


                // pop r32 / pop r32 / ret (+ offset)
                if (2 == curr->treeDepth) {

                    if (ZydisMnemonic::ZYDIS_MNEMONIC_POP == curr->instruction.info.mnemonic &&
                        ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == curr->instruction.operands[0].type)
                    {
                        local = curr->parent;
                        if (ZydisMnemonic::ZYDIS_MNEMONIC_POP == local->instruction.info.mnemonic &&
                            ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == local->instruction.operands[0].type)
                        {
                            type |= GADGET_SEH;
                        }
                    }
                }

                break;
            }
            case GADGET_REDIRECT:
            {
                while (nullptr != local->parent) {
                    local = local->parent;
                }

                ZydisRegister redirectReg = local->instruction.operands[0].reg.value;

                if (1 == curr->treeDepth &&
                    ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == curr->instruction.operands[0].type &&
                    ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == curr->instruction.operands[1].type)
                {

                    switch (curr->instruction.info.mnemonic) {
                        case ZydisMnemonic::ZYDIS_MNEMONIC_XCHG:
                            if (redirectReg == curr->instruction.operands[0].reg.value ||
                                redirectReg == curr->instruction.operands[1].reg.value)
                            {
                                type = GADGET_REDIRECT;
                            }
                            break;
                        case ZydisMnemonic::ZYDIS_MNEMONIC_MOV:
                            if (redirectReg == curr->instruction.operands[0].reg.value) {
                                type = GADGET_REDIRECT;
                            }
                            break;
                        default:
                            type = GADGET_INVALID;
                    }
                }

                else if (2 == curr->treeDepth &&
                    ZydisMnemonic::ZYDIS_MNEMONIC_PUSH == curr->instruction.info.mnemonic &&
                    ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == curr->instruction.operands[0].type)
                {
                    local = curr->parent;
                    if (local && ZydisMnemonic::ZYDIS_MNEMONIC_POP == local->instruction.info.mnemonic &&
                        ZydisOperandType::ZYDIS_OPERAND_TYPE_REGISTER == local->instruction.operands[0].type &&
                        redirectReg == local->instruction.operands[0].reg.value)
                    {
                        type = GADGET_REDIRECT;
                    }
                }
                break;
            }
        }

        return type;
    }

    void WedpGadgetSearch::insert_gadget(WedpGadgetTree* curr, ULONG type)
    {
        WedpGadgetTree* local = curr;
        WEDP_GADGET newgadget;

        newgadget.type = type;
        newgadget.moduleBase = this->module->base;
        newgadget.address = this->memory->base + curr->offset;
        newgadget.rva = newgadget.address - this->module->base;
        newgadget.mnemonic = EXT_INSTANCE.h_Disasm.disasm_format(curr);

        while (nullptr != local) {
            newgadget.len += local->instruction.info.length;
            local = local->parent;
        }

        this->gadgets.push_back(newgadget);
    }
    
    // WedpCache helpers
    bool WedpGadgetHelper::insert(WEDP_GADGET val)
    {
        bool ret = false;
        if (this->exists(val.address)) {
            goto cleanup;
        }

        this->cache.push_back(val);
        ret = true;

    cleanup:
        return ret;
    }

    bool WedpGadgetHelper::exists(ULONG64 val)
    {
        bool ret = false;
        for (WEDP_GADGET mod : this->cache) {
            if (val == mod.address) {
                ret = true;
                goto cleanup;
            }
        }

    cleanup:
        return ret;
    }


    const WEDP_GADGET *WedpGadgetHelper::getvalue(ULONG64 val)
    {
        const WEDP_GADGET* ret = nullptr;

        std::list<WEDP_GADGET>::iterator it = this->cache.begin();
        while (it != this->cache.end())
        {
            if (val == (*it).address) {
                ret = &(*it);
                goto cleanup;
            }

            it++;
        }

    cleanup:
        return ret;
    }


    bool WedpGadgetHelper::remove(ULONG64 val)
    {
        bool ret = false;
        std::list<WEDP_GADGET>::iterator it = this->cache.begin();
        while (it != this->cache.end())
        {
            if (val == (*it).address) {
                this->cache.erase(it);
                ret = true;
                goto cleanup;
            }

            it++;
        }

    cleanup:
        return ret;
    }

    std::vector<WEDP_GADGET> WedpGadgetHelper::filter(WedpGenericOptions opts)
    {
        std::vector<WEDP_GADGET> filtered;
        std::vector<WEDP_MODULE> mods;
        ULONG64 count = 0;

        // Get a full filter on modules
        mods = EXT_INSTANCE.h_Module.filter(opts);

        for (WEDP_GADGET gadget : this->cache) {
            if (!(opts.gadgetType & gadget.type)) {
                continue;
            }

            if (!opts.gadgetRegister.empty()) {
                if (gadget.mnemonic.find(opts.gadgetRegister) == std::string::npos) {
                    continue;
                }
            }

            if (-1 != opts.addressProtections && !(EXT_INSTANCE.h_Memory.getprotect(gadget.address) & opts.addressProtections)) {
                continue;
            }

            // I dont really like this goto here
            WEDP_MODULE curr;
            if (!mods.empty()) {
                for (WEDP_MODULE& mod : mods) {
                    if (mod.base == gadget.moduleBase) {
                        // goto add_gadget;
                        curr = mod;
                        goto next;
                    }
                }
                continue;
            }
        
        next:
            if (opts.addressCharacteristics) {
                bool x86 = (IMAGE_FILE_MACHINE_I386 == curr.arch);
                std::vector<BYTE> address_bytes = EXT_INSTANCE.h_Text.addressToBytes(gadget.address, x86);
             
                if (opts.addressCharacteristics & WedpGenericOptions::ADDRESS_NONULL) {
                    if (!EXT_INSTANCE.h_Text.is_null_free(address_bytes)) {
                        continue;
                    }
                }
                if (opts.addressCharacteristics & WedpGenericOptions::ADDRESS_ASCIIPRINT) {
                    if (!EXT_INSTANCE.h_Text.is_ascii_printable(address_bytes)) {
                        continue;
                    }
                }
                if (opts.addressCharacteristics & WedpGenericOptions::ADDRESS_ASCIIALPHANUM) {
                    if (!EXT_INSTANCE.h_Text.is_ascii_alphanum(address_bytes)) {
                        continue;
                    }
                }
            }

        // add_gadget:
            filtered.push_back(gadget);
            count++;
            if ((ULONG64)(-1) != opts.resultCount && count >= opts.resultCount) break;
        }

        return filtered;
    }

    bool WedpGadgetHelper::checkcachevalidity()
    {
        // The module callbacks set the cache validity
        return this->isvalid();
    }


    bool WedpGadgetHelper::updatecache()
    {
        bool ret = false;
        std::vector<WEDP_MEMORY> exemem;
        std::vector<WedpGadgetSearch*> blocks;
        WedpGenericOptions opts;
        
        if (!this->checkcachevalidity()) {
            DBGPRINT("WedpGadgetHelper updatecache: Need to update\n");
            this->clearcache();
        }
        else {
            DBGPRINT("WedpGadgetHelper updatecache: up to date\n");
            ret = true;
            goto cleanup;
        }

        // Filter the memory cache for executable memory only
        opts.addressProtections = WedpGenericOptions::addProtFlags.at("X");
        exemem = EXT_INSTANCE.h_Memory.filter(opts);
        DBGPRINT("WedpGadgetHelper updatecache: number of executable regions %d\n", exemem.size());

        DBGPRINT("WedpGadgetHelper updatecache: starting gadget search threads\n");
        for (const WEDP_MEMORY &mem : exemem) {
            // See if the executable memory is part of a module, only search if it is
            const WEDP_MODULE* mod = EXT_INSTANCE.h_Module.getvalue(mem.moduleBase);
            if (nullptr != mod) {
                WedpGadgetSearch *curr = new WedpGadgetSearch(&mem, mod);
                blocks.push_back(curr);
                // Start the search trhead
                curr->start_search();
            }
            
        }

        DBGPRINT("WedpGadgetHelper updatecache: waiting on threads to complete and updating cache\n");
        for (WedpGadgetSearch* sb : blocks) {
            if (sb->wait_completion()) {
                this->cache.insert(this->cache.end(), sb->gadgets.begin(), sb->gadgets.end());
            } 
        }
        
        DBGPRINT("WedpGadgetHelper updatecache: cache update complete\n");
        this->cache_is_valid = true;
        ret = true;

    cleanup:
        return ret;
    }

    std::string WedpGadgetHelper::getGadgetTable(WedpGenericOptions opts)
    {
        if (!this->updatecache()) {
            return std::string();
        }

        std::vector<WEDP_GADGET> gadgets = this->filter(opts);

        // sorts the vector using the < operator
        std::sort(gadgets.begin(), gadgets.end());
 
        fort::char_table tbl;

        tbl << fort::header << "Address" << "Instruction" << "Offset" << "Module" << fort::endr;

        for (WEDP_GADGET curr : gadgets) {
            std::stringstream ss;
            const WEDP_MODULE* mod;

            ss << "0x" << std::setfill('0') << std::hex << std::setw(((EXT_INSTANCE.h_Memory.isx64()) ? 64 : 32) / 4) << std::hex << curr.address;
            tbl << ss.str();
            ss.str(std::string());

            tbl << curr.mnemonic;

            ss << "0x" << std::setfill('0') << std::hex << std::setw(8) << std::hex << curr.rva;
            tbl << ss.str();

            mod = EXT_INSTANCE.h_Module.getvalue(curr.moduleBase);
            if (!mod) {
                tbl << "";
                goto endloop;
            }

            tbl << mod->name;

        endloop:
            tbl << fort::endr;
        }

        return tbl.to_string();
    }

} // namespace wedp