#pragma once

// Define to let EngExtCpp know to use our class for its operations
#define EXT_CLASS wedp::Wedp

#include "wedp_asm.h"
#include "wedp_disasm.h"
#include "wedp_options.h"
#include "wedp_text.h"
#include "wedp_module.h"
#include "wedp_exec.h"
#include "wedp_memory.h"
#include "wedp_gadgets.h"
#include "wedp_dbeng_callbacks.h"
#include "wedp_debug.h"

// Added a TODO in dbghelp.h, included in engextcpp.hpp about a broken 
// included header, it is commented out not sure if its needed
#include <engextcpp.hpp>
#include <set>
                                    

namespace wedp {

    // Primary singleton class used by ExtEngCpp to interacte with the dbgeng
    // Holds the primary instances of all helper and data objects
	class Wedp : public ExtExtension {

		public:
			// Initialization  Methods for ExtEngCpp
			HRESULT __thiscall Initialize();
			VOID __thiscall Uninitialize();
            VOID __thiscall OnSessionAccessible(ULONG64 res);

			// Exported commands
            EXT_COMMAND_METHOD(wedp_version);
            EXT_COMMAND_METHOD(wedp_getopts);
            EXT_COMMAND_METHOD(wedp_setopts);
            EXT_COMMAND_METHOD(wedp_pattern_gen);
            EXT_COMMAND_METHOD(wedp_pattern_off);
            EXT_COMMAND_METHOD(wedp_offset);
            
            EXT_COMMAND_METHOD(wedp_asm);
            EXT_COMMAND_METHOD(wedp_disasm);
			
            EXT_COMMAND_METHOD(wedp_modules);
            EXT_COMMAND_METHOD(wedp_memory);
            EXT_COMMAND_METHOD(wedp_rop);
            EXT_COMMAND_METHOD(wedp_seh);
            EXT_COMMAND_METHOD(wedp_redirect);
            EXT_COMMAND_METHOD(wedp_stackpivot);
            EXT_COMMAND_METHOD(wedp_iat);
            

            ULONG64 hProcess;

			// Data classes
            WedpModuleHelper	h_Module;
            WedpMemoryHelper    h_Memory;
            WedpGadgetHelper    h_Gadget;

            // Helper classes
			WedpOptions h_Opts;
            WedpText	h_Text;
			WedpAsm		h_Asm;
            WedpDisasm  h_Disasm;
			WedpExec	h_Exec;

            std::vector<BYTE>* readSessionMemory(ULONG64, ULONG);
            std::string readSessionAscii(ULONG64);
        
        private:
            // Methods to support parsing of arguments
            WedpGenericOptions getGenericOptions();
            

            // Persistent Client for DbgEng callbacks
            IDebugClient* cb_client;
            WedpDbgEngCallbacks* callbacks;
    };
} // namespace wedp