#pragma once
#include "wedp_cache.h"
#include "wedp_options.h"
#include <string>

// This shows that we may need to add more logic, but it also may not matter for our use case
// https://docs.microsoft.com/en-us/windows/win32/memory/memory-limits-for-windows-releases#memory-limits-for-windows-and-windows-server-releases

namespace wedp {

// Macros for easy access of MEMORY_BASIC_INFORMATION32/64 fields
#define GET_MEMORY_FIELD(mbiType, mbi, field) ((mbiType *)&mbi)->field

// Transform a MEMORY_BASIC_INFORMATION32/64 to a WEDP_MEMORY structure
#define CREATE_WEDP_MEMORY(mbiType, mbi, wedpmem)\
    wedpmem.base = GET_MEMORY_FIELD(mbiType, mbi, BaseAddress);\
    wedpmem.size = GET_MEMORY_FIELD(mbiType, mbi, RegionSize);\
    wedpmem.state = GET_MEMORY_FIELD(mbiType, mbi, State);\
    wedpmem.protect = GET_MEMORY_FIELD(mbiType, mbi, Protect);\
    wedpmem.type = GET_MEMORY_FIELD(mbiType, mbi, Type)

    // Hold information about a memory region
    struct WEDP_MEMORY {
        ULONG64 base;
        ULONG64 size;
        DWORD state;
        DWORD protect;
        DWORD type;
        ULONG64 moduleBase;

        // Simple helper to check is this is an executable region
        bool isExecuatble();
    };

    // Used to map memory regions and information about the region that
    // may be of use in exploit writing
    class WedpMemoryHelper : public  WedpCache<WEDP_MEMORY, ULONG64> {

        public:
            std::string getMemoryTable(WedpGenericOptions opts);

            bool insert(WEDP_MEMORY val) override;
            bool exists(ULONG64 val) override;
            const WEDP_MEMORY *getvalue(ULONG64 val) override;
            bool remove(ULONG64 val) override;
            std::vector<WEDP_MEMORY> filter(WedpGenericOptions opts) override;
            bool checkcachevalidity() override;
            bool updatecache() override;


            bool isx64(); 
            DWORD getprotect(ULONG64 addr);

        private:
            bool x64range;
    };

} // namespace wedp