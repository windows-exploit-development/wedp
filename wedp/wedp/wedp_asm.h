#pragma once
#include <Windows.h>
#include <string>
#include <vector>

namespace wedp {

    // WedpAsm contains methods that handle assembling instructions
	class WedpAsm {
		
		public:
            // Assembles newline seperated x86/x64 mnemomics into their machine code instructions
			std::vector<BYTE> assemble(std::string instructions);

            // Replace the input delimeter with newlines so that it can be handled by asmjit
            // returns the output from assemble
            std::vector<BYTE> assemble_delimited(std::string instructions, const char delim = ';');
	};

} // namespace wedp
