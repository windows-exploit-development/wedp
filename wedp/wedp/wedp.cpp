#include "wedp.h"
#include <iostream>
#include <string>

// Instantiates the singleton EXT_CLASS for the duration of the module being loaded
// creates the following globals:
//      wedp::Wedp g_ExtInstance
//      ExtExtension *g_ExtInstanacePtr
EXT_DECLARE_GLOBALS();
#define EXT_INSTANCE g_ExtInstance


namespace wedp {

    // During these initialization calls the dbgeng interfaces are not instatiated
    // so any interaction with dbgeng must happen through self instatiated interfaces

    // Method called when the module is loaded (.load)
	HRESULT __thiscall Wedp::Initialize()
    {
		HRESULT hRet = S_FALSE;
        IDebugControl* Output = NULL;

        // Set the extension version
		this->m_ExtMajorVersion = this->h_Opts.majorVersion;
		this->m_ExtMinorVersion = this->h_Opts.minorVersion;

        // Set up the callbacks for cache validation
        this->callbacks = new WedpDbgEngCallbacks();
        hRet = DebugCreate(__uuidof(IDebugClient), (void**)&this->cb_client);
        if (FAILED(hRet)) {
            goto cleanup;
        }

        hRet = this->cb_client->SetEventCallbacks(this->callbacks);
        if (FAILED(hRet)) {
            goto cleanup;
        }

        // At this point all is good, we are just trying tou output some stuff so we can succeed the load 
        // even if those fail
        hRet = S_OK;

        hRet = DebugCreate(__uuidof(IDebugControl), (PVOID*)&Output);
        if (FAILED(hRet)) {
            OutputDebugStringA("FAILED\n");
            goto cleanup;
        }

        Output->Output(DEBUG_OUTPUT_NORMAL, this->h_Opts.GetVersionString().c_str());
    
    cleanup:
        if (Output) Output->Release();
		return hRet;
	}


    // Method called when module is unloaded (.unload)
	VOID __thiscall Wedp::Uninitialize()
	{
        // Cleanup callbacks
        if (this->cb_client) {
            this->cb_client->Release();
        }
        if (this->callbacks) {
            this->callbacks->Release();
        }
        
		return;
	}


    // Methods used for notification of debugger state

    // Called when the debuggee is suspended and the session is accessible, must manually initialize the interfaces with Query
    VOID Wedp::OnSessionAccessible(ULONG64 res)
    {
        UNREFERENCED_PARAMETER(res);

        IDebugClient* localclient = NULL;
        
        if (FAILED(DebugCreate(__uuidof(IDebugClient), (void**)&localclient))) {
            goto cleanup;
        }

        if (FAILED(EXT_INSTANCE.Query(localclient))) {
            goto cleanup;
        }

        EXT_INSTANCE.m_System->GetCurrentProcessHandle(&(this->hProcess));

        // Call update cache methods on all caches, will only take time if the cache has
        // been invalidated
        EXT_INSTANCE.h_Module.updatecache();
        EXT_INSTANCE.h_Memory.updatecache();
        // NOTE: commented out for the sake of speed on load right now, could be done in a background thread
        // g_ExtInstance.h_Gadget.updatecache();

    cleanup:
        if (*&(EXT_INSTANCE.m_Advanced) != NULL) {
            EXT_INSTANCE.Release();
        }
        if (localclient) {
            localclient->Release();
        }
        return;
    }


    WedpGenericOptions Wedp::getGenericOptions()
    {
        // Get a copy of the global options
        WedpGenericOptions ret = EXT_INSTANCE.h_Opts.g_opts;

        // Get the result count argument
        if (this->HasArg("c")) {
            ret.resultCount = this->GetArgU64("c");
        }

        // Get the output directory argument
        if (this->HasArg("w")) {
            ret.outputDir = std::string(this->GetArgStr("w"));

            // Remove the quotes from the outside of the path
            this->h_Text.strip(ret.outputDir, '"');
            this->h_Text.strip_back(ret.outputDir, '\\');
        }

        // Get the output file argument
        if (this->HasArg("o")) {
            ret.outputFile = std::string(this->GetArgStr("o"));
            
            // Remove the quotes from the outside of the path
            this->h_Text.strip(ret.outputFile, '"');
        }

        // Can only have include or exclude, not both
        if (this->HasArg("mi") && this->HasArg("me")) {
            // Error here
        }
        else if (this->HasArg("mi")) {
            std::string arg = std::string(this->GetArgStr("mi"));
            std::vector<std::string> tmp;
            ret.modulesFlags |= WedpGenericOptions::MODULE_FLAG_INCLUDE;
            tmp = g_ExtInstance.h_Text.split(arg);
            for (std::string& mod : tmp) {
                g_ExtInstance.h_Text.tolower(mod);
            }
            ret.modules = std::set<std::string>(tmp.begin(), tmp.end());
        }
        else if (this->HasArg("me")) {
            std::string arg = std::string(this->GetArgStr("me"));
            std::vector<std::string> tmp;
            ret.modulesFlags |= WedpGenericOptions::MODULE_FLAG_EXCLUDE;
            tmp = g_ExtInstance.h_Text.split(arg);
            ret.modules = std::set<std::string>(tmp.begin(), tmp.end());
        }

        if (this->HasArg("mc")) {
            // Format - option=[t|f|*]
            // Options - aslr,dep,safeseh,cfg
            std::string arg = std::string(this->GetArgStr("mc"));
            std::vector<std::string> opts = { "aslr", "dep", "safeseh", "cfg", "os" };

            std::vector<std::string> ind = g_ExtInstance.h_Text.split(arg);
            for (std::string curropt : ind) {
                std::size_t off;
                
                off = curropt.find('=');
                if (std::string::npos == off) {
                    continue;
                }

                std::string optstr = curropt.substr(0, off);
                std::string optval = curropt.substr(off + 1, curropt.size() - (off + 1));

                g_ExtInstance.h_Text.tolower(optstr);
                g_ExtInstance.h_Text.tolower(optval);

                // If the option value is * we dont set any flags
                if (0 == optval.compare("*")) {
                    continue;
                }

                // This is some bad parsing, im sure it could be done way better with a lookup table possibly
                // NOTE: could use a lookup table to check the full option=value part
                if (0 == optstr.compare("aslr")) {
                    if (0 == optval.compare("t")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_ASLR_ENABLED;
                    }
                    else if (0 == optval.compare("f")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_ASLR_DISABLED;
                    }
                }
                else if (0 == optstr.compare("dep")) {
                    if (0 == optval.compare("t")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_DEP_ENABLED;
                    }
                    else if (0 == optval.compare("f")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_DEP_DISABLED;
                    }
                }
                else if (0 == optstr.compare("safeseh")) {
                    if (0 == optval.compare("t")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_SAFESEH_ENABLED;
                    }
                    else if (0 == optval.compare("f")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_SAFESEH_DISABLED;
                    }
                }
                else if (0 == optstr.compare("cfg")) {
                    if (0 == optval.compare("t")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_CFG_ENABLED;
                    }
                    else if (0 == optval.compare("f")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_CFG_DISABLED;
                    }
                }  
                else if (0 == optstr.compare("os")) {
                    if (0 == optval.compare("t")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_SYSTEM_IMAGE_TRUE;
                    }
                    else if (0 == optval.compare("f")) {
                        ret.modulesFlags |= WedpGenericOptions::MODULE_SYSTEM_IMAGE_FALSE;
                    }
                }
            }
        }

        if (this->HasArg("ap")) {
            std::string arg = std::string(this->GetArgStr("ap"));
            if (0 != arg.compare("*")) {
                try {
                    ret.addressProtections = WedpGenericOptions::addProtFlags.at(arg);
                }
                catch (std::out_of_range e) {
                    ret.addressProtections = 0xffffffff;
                }
            }
        }

        if (this->HasArg("ab")) {
            std::string arg = std::string(this->GetArgStr("ab"));
            std::vector<BYTE> chars = g_ExtInstance.h_Text.hexToBytes(arg);
            ret.addressBadChars = std::set<BYTE>(chars.begin(), chars.end());
        }
       
        if (this->HasArg("ac")) {
            // Format - option,option,option
            // Options - nonull,print,alphanum
            std::string arg = std::string(this->GetArgStr("ac"));
            std::vector<std::string> opts = { "nonull", "print", "alphanum" };

            std::vector<std::string> ind = g_ExtInstance.h_Text.split(arg);
            for (std::string curropt : ind) {
                g_ExtInstance.h_Text.tolower(curropt);
                if (0 == curropt.compare(opts[0])) {
                    ret.addressCharacteristics |= WedpGenericOptions::ADDRESS_NONULL;
                }
                else if (0 == curropt.compare(opts[1])) {
                    ret.addressCharacteristics |= WedpGenericOptions::ADDRESS_ASCIIPRINT;
                }
                else if (0 == curropt.compare(opts[2])) {
                    ret.addressCharacteristics |= WedpGenericOptions::ADDRESS_ASCIIALPHANUM;
                }
            }
        }

        if (this->HasArg("gr")) {
            std::string arg = std::string(this->GetArgStr("gr"));
            ret.gadgetRegister = arg;
        }

        return ret;
    }

    
    std::vector<BYTE>* Wedp::readSessionMemory(ULONG64 address, ULONG size)
    {
        std::vector<BYTE>* ret = new std::vector<BYTE>(size);
        SIZE_T count = 0;

        if (!ReadProcessMemory((HANDLE)this->hProcess, (LPCVOID)address, ret->data(), ret->size(), &count)) {
            goto error;
        }

        if (count != size) {
            goto error;
        }

        goto complete;

    error:
        if (ret) {
            delete ret;
            ret = nullptr;
        }

    complete:
        return ret;
    }

    std::string Wedp::readSessionAscii(ULONG64 address)
    {
        std::string ret;
        ULONG offset = 0;
        UCHAR curr = 0;
        SIZE_T read = 0;

        
        for (;;) {
            if (!ReadProcessMemory((HANDLE)this->hProcess, (LPCSTR)(address + offset), &curr, 1, &read) || 0 == read) {
                ret = std::string();
                goto cleanup;
            }

            if (curr == 0) {
                goto cleanup;
            }

            ret.push_back(curr);
            offset++;
        }
        

    cleanup:
        return ret;
    }

} // namespace wedp