#include <Zydis/zydis.h>
#include <inttypes.h>
#include "wedp.h"
#include "wedp_gadgets.h"
#include "wedp_disasm.h"

extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

    WedpDisasm::WedpDisasm()
    {
    }


    WedpGadgetTree* WedpDisasm::disasm_instruction(std::vector<BYTE>& bytes, ULONG offset, WORD machine)
    {
        WedpGadgetTree* ret = new WedpGadgetTree();
        ZydisMachineMode mode = ZYDIS_MACHINE_MODE_LONG_64;

        if (IMAGE_FILE_MACHINE_I386 == machine) {
            mode = ZYDIS_MACHINE_MODE_LONG_COMPAT_32;
        }

        ret->machine = machine;
        ret->offset = offset;
        ret->parent = nullptr;
        ret->treeDepth = 0;

        // Decode the instruction at offset
        if (ZYAN_SUCCESS(ZydisDisassembleIntel(mode, 0, bytes.data() + offset, bytes.size() - offset, &ret->instruction))) {
            goto cleanup;
        }

        // On error cleanup memory and return null
        delete ret;
        ret = nullptr;

    cleanup:
        return ret;
    }


    bool WedpDisasm::disasm_backwards(std::vector<BYTE>& bytes, ULONG depth, WedpGadgetTree* root)
    {
        // Disassemble backwards to build a tree of gadgets from a specified root
        // REF: https://github.com/defuse/gadgetrie?files=1
        bool ret = false;
        std::queue<WedpGadgetTree*> search_entries;
        ZydisMachineMode mode = ZYDIS_MACHINE_MODE_LONG_64;

        if (IMAGE_FILE_MACHINE_I386 == root->machine) {
            mode = ZYDIS_MACHINE_MODE_LONG_COMPAT_32;
        }

        search_entries.push(root);

        // Continue while we have start offsets in the queue
        while (!search_entries.empty()) {
            
            // Get the next offset
            WedpGadgetTree* currparent = search_entries.front();
            search_entries.pop();

            UINT startoffset = currparent->offset;
            UINT instlen = 1;

            for (; instlen <= MAX_USEFUL_INST_LEN; instlen++) {
                
                // Need a signed comparison
                INT curroffset = (INT)(startoffset - instlen);
                if (0 > curroffset) {
                    break;
                }

                WedpGadgetTree* currchild = this->disasm_instruction(bytes, curroffset, root->machine);
                if (nullptr != currchild) {
                    if (currchild->instruction.info.length == instlen &&
                        !this->disasm_short_circuit(currchild->instruction))
                    {
                        currchild->parent = currparent;
                        currchild->treeDepth = currparent->treeDepth + 1;
                        currparent->children.push_back(currchild);
                        if (currchild->treeDepth < depth) {
                            search_entries.push(currchild);
                        }
                    }
                    else {
                        delete currchild;
                    }

                    currchild = nullptr;
                }
            }
        }

        ret = true;

        return ret;
    }

    
    std::string WedpDisasm::disasm_format(WedpGadgetTree* inst)
    {
        WedpGadgetTree* curr = inst;
        std::string formatted;

        while (nullptr != curr) {
            formatted.append(curr->instruction.text);
            formatted.append("; ");

            curr = curr->parent;
        }

        formatted.erase(formatted.end() - 2, formatted.end());
        return formatted;
    }
    
   
    ULONG WedpDisasm::disasm_operand_count(WedpGadgetTree* curr)
    {
        ULONG count = 0;
        while (count < curr->instruction.info.operand_count) {
            if (ZydisOperandVisibility::ZYDIS_OPERAND_VISIBILITY_EXPLICIT != curr->instruction.operands[count].visibility) {
                break;
            }
            count++;
        }
        return count;
    }
    
    
    bool WedpDisasm::disasm_short_circuit(ZydisDisassembledInstruction inst)
    {
        switch (inst.info.mnemonic) {
            case ZYDIS_MNEMONIC_RET:
            case ZYDIS_MNEMONIC_JMP:
            case ZYDIS_MNEMONIC_CALL:
            case ZYDIS_MNEMONIC_LEAVE:
            case ZYDIS_MNEMONIC_INT3:
                return true;
            default:
                return false;
        }
    }


    void WedpDisasm::disassemble(std::string bytestr)
    {
        std::vector<BYTE> bytes = EXT_INSTANCE.h_Text.hexToBytes(bytestr);
        ZydisDisassembledInstruction instruction;
        ZydisMachineMode mode = ZYDIS_MACHINE_MODE_LONG_64;
        ZyanUSize offset = 0;

        if (IMAGE_FILE_MACHINE_I386 == EXT_INSTANCE.GetEffectiveProcessor()) {
            mode = ZYDIS_MACHINE_MODE_LONG_COMPAT_32;
        }

        while (ZYAN_SUCCESS(ZydisDisassembleIntel(mode, 0, bytes.data() + offset, bytes.size() - offset, &instruction)))
        {  
            EXT_INSTANCE.Out("%s\n", instruction.text);
            offset += instruction.info.length;
        }

    }

} // namespace wedp