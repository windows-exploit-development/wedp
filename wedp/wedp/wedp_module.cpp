#pragma warning(disable: 4127)
#include <fort.hpp>
#pragma warning(default: 4127)

#include "wedp.h"

#include <Psapi.h>
#include <sstream>
#include <iomanip>

extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

#define MAX_DRIVE_BUFFER 26 * 4

    // Flags that show protections on a binary
    enum {
        ASLR_ENABLED = (1 << 0),
        NX_ENABLED = (1 << 1),
        CFG_ENABLED = (1 << 2),
        SAFESEH_ENABLED = (1 << 3),
        IMAGE_REBASED = (1 << 4),
        SYSTEM_IMAGE = (1 << 5),
    };


    // Used to get the version info of a module
    typedef struct _LANGANDCODEPAGE {
        WORD wLanguage;
        WORD wCodePage;
    } LANGANDCODEPAGE;


    // WedpCache Methods
    bool WedpModuleHelper::insert(WEDP_MODULE val)
    {
        bool ret = false;
        if (this->exists(val.base)) {
            goto cleanup;
        }

        this->cache.push_back(val);
        ret = true;

    cleanup:
        return ret;
    }


    bool WedpModuleHelper::exists(ULONG64 val)
    {
        bool ret = false;
        for (WEDP_MODULE mod : this->cache) {
            if (val == mod.base) {
                ret = true;
                goto cleanup;
            }
        }

    cleanup:
        return ret;
    }


    const WEDP_MODULE *WedpModuleHelper::getvalue(ULONG64 val)
    {
        const WEDP_MODULE* ret = nullptr;

        std::list<WEDP_MODULE>::iterator it = this->cache.begin();
        while (it != this->cache.end())
        {
            if (val == (*it).base) {
                ret = &(*it);
                goto cleanup;
            }

            it++;
        }

    cleanup:
        return ret;
    }


    bool WedpModuleHelper::remove(ULONG64 val)
    {
        bool ret = false;
        std::list<WEDP_MODULE>::iterator it = this->cache.begin();
        while (it != this->cache.end())
        {
            if (val == (*it).base) {
                this->cache.erase(it);
                ret = true;
                goto cleanup;
            }

            it++;
        }

    cleanup:
        return ret;
    }

    std::vector<WEDP_MODULE> WedpModuleHelper::filter(WedpGenericOptions opts)
    {
        std::vector<WEDP_MODULE> filtered;
        ULONG64 count = 0;

        for (WEDP_MODULE mod : this->cache) {

            // Check the include and exclude lists
            if ((opts.modulesFlags & WedpGenericOptions::MODULE_FLAG_EXCLUDE && opts.modules.find(mod.name) != opts.modules.end()) ||
                (opts.modulesFlags & WedpGenericOptions::MODULE_FLAG_INCLUDE && opts.modules.find(mod.name) == opts.modules.end()))
            {
                continue;
            }

            // Check ASLR
            if ((opts.modulesFlags & WedpGenericOptions::MODULE_ASLR_DISABLED && mod.protections & ASLR_ENABLED) ||
                (opts.modulesFlags & WedpGenericOptions::MODULE_ASLR_ENABLED && !(mod.protections & ASLR_ENABLED)))
            {
                continue;
            }

            // Check DEP
            if ((opts.modulesFlags & WedpGenericOptions::MODULE_DEP_DISABLED && mod.protections & NX_ENABLED) ||
                (opts.modulesFlags & WedpGenericOptions::MODULE_DEP_ENABLED && !(mod.protections & NX_ENABLED)))
            {
                continue;
            }

            // Check SAFESEH
            if ((opts.modulesFlags & WedpGenericOptions::MODULE_SAFESEH_DISABLED && mod.protections & SAFESEH_ENABLED) ||
                (opts.modulesFlags & WedpGenericOptions::MODULE_SAFESEH_ENABLED && !(mod.protections & SAFESEH_ENABLED)))
            {
                continue;
            }

            // Check CFG
            if ((opts.modulesFlags & WedpGenericOptions::MODULE_CFG_DISABLED && mod.protections & CFG_ENABLED) ||
                (opts.modulesFlags & WedpGenericOptions::MODULE_CFG_ENABLED && !(mod.protections & CFG_ENABLED)))
            {
                continue;
            }

            // Check system image
            if ((opts.modulesFlags & WedpGenericOptions::MODULE_SYSTEM_IMAGE_FALSE && mod.protections & SYSTEM_IMAGE) ||
                (opts.modulesFlags & WedpGenericOptions::MODULE_SYSTEM_IMAGE_TRUE && !(mod.protections & SYSTEM_IMAGE)))
            {
                continue;
            }

            
            filtered.push_back(mod);
            count++;
            if ((ULONG64)(-1) != opts.resultCount && count >= opts.resultCount) break;
        }

        return filtered;
    }

    // WedpCache Functions
    bool WedpModuleHelper::checkcachevalidity() 
    {
        ULONG currPid;
        if (FAILED(EXT_INSTANCE.m_System->GetCurrentProcessSystemId(&currPid))) {
            return false;
        }

        if (currPid != this->cachedPid) { 
            this->cache_is_valid = false;
            this->cachedPid = currPid;
            this->cachedPointerLen = 32;
        }

        // The module callbacks will invalidate cache on load or unload so we dont 
        // need to worry about that here

        return this->isvalid();
    }

    bool WedpModuleHelper::updatecache()
    {
        bool ret = false;
        ULONG loaded = 0;
        ULONG tmp = 0; 
        
        // Check if the cache is valid, return if it is and clear it if it is not
        if (!this->checkcachevalidity()) {
            this->clearcache();
        }
        else {
            ret = true;
            goto cleanup;
        }

        if (FAILED(EXT_INSTANCE.m_Symbols->GetNumberModules(&loaded, &tmp))) {
            goto cleanup;
        }

        for (tmp = 0; tmp < loaded; tmp++) {
            // Reload the data for each module in the current process
            WEDP_MODULE data;
            ULONG64 base = 0;

            if (FAILED(EXT_INSTANCE.m_Symbols->GetModuleByIndex(tmp, &base))) {
                goto cleanup;
            }

            if (false == this->parseModule(base, data)) {
                goto cleanup;
            }

            this->insert(data);
        }

        this->cache_is_valid = true;
        ret = true;

    cleanup:
        return ret;
    }
    

    // Public functions
    std::string WedpModuleHelper::getModulesTable(WedpGenericOptions opts)
    {
        if (!this->updatecache()) {
            return std::string();
        }

        std::vector<WEDP_MODULE> mods = this->filter(opts);
        fort::char_table tbl;

        tbl << fort::header << "Name" << "Machine" << "Base" << "Size" << "OS" <<  "Rebased" << "ASLR"
            << "DEP" << "CFG" << "SAFESEH" << "Version" << "Path" << fort::endr;

        for (WEDP_MODULE modData : mods) {
            std::stringstream ss;

            tbl << modData.name.c_str();
            tbl << ((IMAGE_FILE_MACHINE_I386 == modData.arch) ? "x86" : "x64");

            ss << "0x" << std::setfill('0') << std::hex << std::setw(this->cachedPointerLen / 4) << std::hex << modData.base;
            tbl << ss.str();
            ss.str(std::string());

            ss << "0x" << std::setfill('0') << std::hex << std::setw(8) << std::hex << modData.size;
            tbl << ss.str();
            ss.clear();

            tbl << ((modData.protections & SYSTEM_IMAGE) ? "True" : "False");
            tbl << ((modData.protections & IMAGE_REBASED) ? "True" : "False");
            tbl << ((modData.protections & ASLR_ENABLED) ? "True" : "False");
            tbl << ((modData.protections & NX_ENABLED) ? "True" : "False");
            tbl << ((modData.protections & CFG_ENABLED) ? "True" : "False");
            tbl << ((modData.protections & SAFESEH_ENABLED) ? "True" : "False");

            tbl << modData.version;
            tbl << modData.path;
            tbl << fort::endr;
        }

        return tbl.to_string();
    }

    std::string WedpModuleHelper::getModulesIat(WedpGenericOptions opts)
    {
        if (!this->updatecache()) {
            return std::string();
        }

        std::vector<WEDP_MODULE> mods = this->filter(opts);
        fort::char_table tbl;

        tbl << fort::header << "Module" << "Import Module" << "Function" << "Function Address" << "IAT Address" << fort::endr;

        for (WEDP_MODULE modData : mods) {
            for (WedpModuleImport imp : modData.imports) {
                std::stringstream ss;

                tbl << modData.name;
                tbl << imp.moduleName;
                tbl << imp.functionName;

                ss << "0x" << std::setfill('0') << std::hex << std::setw(this->cachedPointerLen / 4) << std::hex << imp.importAddress;
                tbl << ss.str();
                ss.str(std::string());

                ss << "0x" << std::setfill('0') << std::hex << std::setw(this->cachedPointerLen / 4) << std::hex << imp.localAddress;
                tbl << ss.str();
                ss.str(std::string());

                tbl << fort::endr;
            }       
        }

        return tbl.to_string();
    }


    // Get the base address of a module the input address is located in
    ULONG64 WedpModuleHelper::getModuleBase(ULONG64 addr)
    {
        ULONG64 ret = (ULONG64)(-1);
        
        // Make sure our module cache is up to date
        if (!this->updatecache()) {
            goto cleanup;
        }

        for (WEDP_MODULE modData : this->cache) {
            if (addr >= modData.base && addr < (modData.base + modData.size)) {
                ret = modData.base;
                goto cleanup;
            }
        }
    
    cleanup:
        return ret;
    }
    
    bool WedpModuleHelper::parseModule(ULONG64 base, WEDP_MODULE& data)
    {
        BOOL bRet = FALSE;
        DEBUG_MODULE_PARAMETERS modParams = { 0 };
        ULONG ulcb = 0;
        ULONG cbname = 0;
        std::vector<CHAR> nameBuff;
        IMAGE_DOS_HEADER dosHdr = { 0 };
        IMAGE_NT_HEADERS64 ntHdr64 = { 0 };
        PIMAGE_NT_HEADERS32 ntHdr32 = NULL;
        IMAGE_LOAD_CONFIG_DIRECTORY32 loadConfig32 = { 0 };
        WORD wDllChars = 0;
        UCHAR prots = 0;
        std::vector<LANGANDCODEPAGE> langVec;
        std::vector<CHAR> fileInfo;
        std::stringstream sfinfo;
        std::vector<WedpModuleImport> imports;
        std::string fullPath;
        ULONG64 prefferedBase = 0;
        

        if (FAILED(EXT_INSTANCE.m_Symbols->GetModuleParameters(1, &base, 0, &modParams))) {
            goto cleanup;
        }

        // Get the path and module name
        fullPath = this->getFullModulePath(base);
        nameBuff = std::vector<CHAR>(modParams.ModuleNameSize);
        if (FAILED(EXT_INSTANCE.m_Symbols->GetModuleNames(DEBUG_ANY_ID, base,
            NULL, 0, NULL,
            nameBuff.data(), (ULONG)nameBuff.size(), &cbname,
            NULL, 0, NULL)))
        {
            goto cleanup;
        }

        // NOTE: potetntially use file version below and check for WinBuild
        if (fullPath.find("Windows") != std::string::npos) {
            prots |= SYSTEM_IMAGE;
        }

        // Get the IMAGE_DOS_HEADER
        if (FAILED(EXT_INSTANCE.m_Data->ReadVirtual(base, &dosHdr, sizeof(dosHdr), &ulcb))) {
            goto cleanup;
        }

        // Get the IMAGE_NT_HEADERS.
        // Use IMAGE_NT_HEADERS64 as a container as it is bigger than IMAGE_NT_HEADERS32, could use a untion here
        if (FAILED(EXT_INSTANCE.m_Data->ReadVirtual(base + dosHdr.e_lfanew, &ntHdr64, sizeof(ntHdr64), &ulcb))) {
            goto cleanup;
        }

        

        if (IMAGE_FILE_MACHINE_I386 == ntHdr64.FileHeader.Machine) {
            ntHdr32 = (PIMAGE_NT_HEADERS32)& ntHdr64;
        }
        else {
            this->cachedPointerLen = 64;
        }

        // Check against image from disk to see if its rebased
        prefferedBase = getModulePreferredBase(fullPath);
        prots |= (prefferedBase != base) ? IMAGE_REBASED : 0;

        // Get DllCharacteristics to see what protection flags are set
        wDllChars = (ntHdr32) ? ntHdr32->OptionalHeader.DllCharacteristics : ntHdr64.OptionalHeader.DllCharacteristics;
        prots |= (wDllChars & IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE) ? ASLR_ENABLED : 0;
        prots |= (wDllChars & IMAGE_DLLCHARACTERISTICS_NX_COMPAT) ? NX_ENABLED : 0;
        prots |= (wDllChars & IMAGE_DLLCHARACTERISTICS_GUARD_CF) ? CFG_ENABLED : 0;
        

        // The following checks if SAFESEH is enabled which only matters on x86
        if (ntHdr32 &&
            ntHdr32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].Size != 0 &&
            ntHdr32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].VirtualAddress != 0)
        {
            if (FAILED(EXT_INSTANCE.m_Data->ReadVirtual(base + ntHdr32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].VirtualAddress,
                &loadConfig32, sizeof(loadConfig32), &ulcb)))
            {
                goto cleanup;
            }

            if (loadConfig32.SEHandlerCount > 0 ||
                loadConfig32.SEHandlerTable != NULL)
            {
                prots |= SAFESEH_ENABLED;
            }
        }

        // Parse imports
        imports = this->getModuleImports(base, &ntHdr64, (NULL == ntHdr32));

        // All this bullshit to get the file version
        data.version = std::string("No Version Info");
        if (FAILED(EXT_INSTANCE.m_Symbols2->GetModuleVersionInformation(DEBUG_ANY_ID, base, "\\VarFileInfo\\Translation", NULL, 0, &ulcb))) {
            goto setdata;
        }

        langVec = std::vector<LANGANDCODEPAGE>(ulcb / sizeof(LANGANDCODEPAGE));

        if (FAILED(EXT_INSTANCE.m_Symbols2->GetModuleVersionInformation(DEBUG_ANY_ID, base, "\\VarFileInfo\\Translation", &langVec[0], ulcb, NULL))) {
            goto setdata;
        }

        sfinfo << "\\StringFileInfo\\";
        sfinfo << std::hex << std::setw(4) << std::setfill('0') << langVec[0].wLanguage;
        sfinfo << std::hex << std::setw(4) << std::setfill('0') << langVec[0].wCodePage;
        // sfinfo << "\\" << "FileVersion"; // ProductVersion doesnt include winbuild
        sfinfo << "\\" << "ProductVersion";

        ulcb = 0;
        if (FAILED(EXT_INSTANCE.m_Symbols2->GetModuleVersionInformation(DEBUG_ANY_ID, base, sfinfo.str().c_str(), NULL, 0, &ulcb))) {
            if (!ulcb) {
                goto setdata;
            }
        }

        fileInfo = std::vector<CHAR>(ulcb + 1);
        if (FAILED(EXT_INSTANCE.m_Symbols2->GetModuleVersionInformation(DEBUG_ANY_ID, base, sfinfo.str().c_str(), fileInfo.data(), ulcb + 1, NULL))) {
            goto setdata;
        }

        data.version = std::string(fileInfo.begin(), fileInfo.end());


        // Load up our datastructure
    setdata:
        data.base = base;
        data.preferredBase = prefferedBase;
        data.size = modParams.Size;
        data.name = std::string(nameBuff.begin(), nameBuff.end() - 1);
        EXT_INSTANCE.h_Text.tolower(data.name);
        data.path = fullPath;
        data.arch = ntHdr64.FileHeader.Machine;
        data.protections = prots;
        data.imports = imports;
        bRet = TRUE;

    cleanup:
        return bRet;
    }

    std::string WedpModuleHelper::getFullModulePath(ULONG64 base)
    {
        std::string ret = std::string();
        ULONG64 pHandle;
        CHAR rootPath[MAX_PATH] = { 0 };
        CHAR driveLetters[MAX_DRIVE_BUFFER] = { 0 };
        CHAR currDriveDevice[MAX_PATH] = { 0 };
        CHAR* rootPos = rootPath;
        CHAR* drivePos = driveLetters;
        
        // Get the handle to the process being debugged
        if (FAILED(EXT_INSTANCE.m_System->GetCurrentProcessHandle(&pHandle))) {
            goto cleanup;
        }

        // Get the DOS path of the loaded module, need to convert the DosDevice to the drive letter
        if (0 == GetMappedFileNameA((HANDLE)pHandle, (LPVOID)base, (LPSTR)rootPath, MAX_PATH)) {
            goto cleanup;
        }

        // Get the drive letters
        DWORD size = GetLogicalDriveStringsA(MAX_DRIVE_BUFFER, (LPSTR)driveLetters);
        if (0 == size) {
            goto cleanup;
        }

        while (drivePos < (driveLetters + size)) {
            // Get rid of the \ from the drive letter
            DWORD len = (DWORD)strlen(drivePos);
            drivePos[len - 1] = 0;

            SecureZeroMemory(currDriveDevice, MAX_PATH);
            // Get the dos device of the current drive letter
            if (0 == QueryDosDeviceA(drivePos, (LPSTR)currDriveDevice, MAX_PATH)) {
                goto next_iter;
            }

            // Check the current dos device against the device of the mapped image
            if (0 == strncmp(rootPath, currDriveDevice, strlen(currDriveDevice))) {
                // On match we copy the drive letter to the mapped image path at the
                // end of the dos device name - the size of the drive (in theory always 2)
                // then rootPath is pointer to the the new path using a drive letter
                rootPos += strlen(currDriveDevice) - strlen(drivePos);
                CopyMemory(rootPos, drivePos, strlen(drivePos));
                ret = std::string(rootPos);
                break;
            }

        next_iter:
            drivePos += len + 1;
        }
      
    cleanup:
        return ret;
    }

    ULONG64 WedpModuleHelper::getModulePreferredBase(std::string filepath)
    {
        ULONG64 ret = 0;
        DWORD dwFileSize = 0;
        DWORD dwRead = 0;
        LPVOID fileBuff = NULL;
        PIMAGE_NT_HEADERS64 nt64 = NULL;
        PIMAGE_NT_HEADERS32 nt32 = NULL;
        HANDLE hFile = INVALID_HANDLE_VALUE;

        hFile = CreateFileA(filepath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
        if (INVALID_HANDLE_VALUE == hFile) {
            goto cleanup;
        }

        dwFileSize = GetFileSize(hFile, NULL);
        fileBuff = HeapAlloc(GetProcessHeap(), 0, dwFileSize);
        if (NULL == fileBuff) {
            goto cleanup;
        }

        if (!ReadFile(hFile, fileBuff, dwFileSize, &dwRead, NULL)) {
            goto cleanup;
        }

        nt64 = (PIMAGE_NT_HEADERS64)((SIZE_T)fileBuff + ((PIMAGE_DOS_HEADER)fileBuff)->e_lfanew);
        if (IMAGE_FILE_MACHINE_I386 == nt64->FileHeader.Machine) {
            nt32 = (PIMAGE_NT_HEADERS32)nt64;
            ret = nt32->OptionalHeader.ImageBase;;
        }
        else {
            ret = nt64->OptionalHeader.ImageBase;
        }

    cleanup:
        if (fileBuff) {
            HeapFree(GetProcessHeap(), 0, fileBuff);
        }
        if (INVALID_HANDLE_VALUE != hFile) {
            CloseHandle(hFile);
        }
        return ret;
    }

    std::vector<WedpModuleImport> WedpModuleHelper::getModuleImports(ULONG64 base, PIMAGE_NT_HEADERS64 ntHdrs, bool x64)
    {
        IMAGE_DATA_DIRECTORY entry = { 0 };
        std::vector<WedpModuleImport> ret;
        std::vector<BYTE>* descVec = nullptr;
        std::vector<BYTE>* nameVec = nullptr;
        PIMAGE_IMPORT_DESCRIPTOR desc = NULL;

        if (!x64) {
            entry = ((PIMAGE_NT_HEADERS32)ntHdrs)->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
        }
        else {
            entry = ntHdrs->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
        }

        if (0 == entry.VirtualAddress || 0 == entry.Size) {
            goto cleanup;
        }

        descVec = EXT_INSTANCE.readSessionMemory(base + entry.VirtualAddress, entry.Size);
        if (!descVec) {
            // error
        }
        desc = (PIMAGE_IMPORT_DESCRIPTOR)descVec->data();
       
        for (; 0 != desc->Characteristics; desc++) {
            std::string modname = EXT_INSTANCE.readSessionAscii(base + desc->Name);

            if (x64) {
                std::vector<BYTE>* origThunkVec = EXT_INSTANCE.readSessionMemory(base + desc->OriginalFirstThunk, sizeof(IMAGE_THUNK_DATA64));
                std::vector<BYTE>* thunkVec = EXT_INSTANCE.readSessionMemory(base + desc->FirstThunk, sizeof(IMAGE_THUNK_DATA64));
                PIMAGE_THUNK_DATA64 thunk = (PIMAGE_THUNK_DATA64)origThunkVec->data();
                ULONG64 index = 0;
                ULONG64 addr = 0;

                while (0 != thunk->u1.AddressOfData) {
                    WedpModuleImport ci;
                    std::stringstream fns;
                    std::string fname;

                    ci.moduleName = modname;

                    if (IMAGE_ORDINAL_FLAG32 & thunk->u1.Ordinal) {
                        fns << "ordinal_" << std::hex << (UINT16)(thunk->u1.Ordinal & ~IMAGE_ORDINAL_FLAG32);
                        fname = fns.str();
                    }
                    else {
                        fname = EXT_INSTANCE.readSessionAscii(base + thunk->u1.AddressOfData + 2);
                    }

                    
                    memcpy(&addr, thunkVec->data(), sizeof(IMAGE_THUNK_DATA64));

                    ci.functionName = fname;
                    ci.importAddress = addr;
                    ci.localAddress = base + desc->FirstThunk + (index * sizeof(IMAGE_THUNK_DATA64));
                    ret.push_back(ci);

                    delete thunkVec;
                    delete origThunkVec;

                    index++;
                    origThunkVec = EXT_INSTANCE.readSessionMemory(base + desc->OriginalFirstThunk + (index * sizeof(IMAGE_THUNK_DATA64)), sizeof(IMAGE_THUNK_DATA64));
                    thunkVec = EXT_INSTANCE.readSessionMemory(base + desc->FirstThunk + (index * sizeof(IMAGE_THUNK_DATA64)), sizeof(IMAGE_THUNK_DATA64));
                    thunk = (PIMAGE_THUNK_DATA64)origThunkVec->data();
                }
            }
            else {
                std::vector<BYTE>* origThunkVec = EXT_INSTANCE.readSessionMemory(base + desc->OriginalFirstThunk, sizeof(IMAGE_THUNK_DATA32));
                std::vector<BYTE>* thunkVec = EXT_INSTANCE.readSessionMemory(base + desc->FirstThunk, sizeof(IMAGE_THUNK_DATA32));
                PIMAGE_THUNK_DATA32 thunk = (PIMAGE_THUNK_DATA32)origThunkVec->data();
                ULONG index = 0;
                ULONG64 addr = 0;

                while (0 != thunk->u1.AddressOfData) {
                    WedpModuleImport ci;
                    std::stringstream fns;
                    std::string fname;

                    ci.moduleName = modname;
                    
                    if (IMAGE_ORDINAL_FLAG32 & thunk->u1.Ordinal) {
                        fns << "ordinal_" << std::hex << (UINT16)(thunk->u1.Ordinal & ~IMAGE_ORDINAL_FLAG32);
                        fname = fns.str();
                    }
                    else {
                        fname = EXT_INSTANCE.readSessionAscii(base + thunk->u1.AddressOfData + 2);
                    }

                    memcpy(&addr, thunkVec->data(), sizeof(IMAGE_THUNK_DATA32));

                    ci.functionName = fname;
                    ci.importAddress = addr;
                    ci.localAddress = base + desc->FirstThunk + (index * sizeof(IMAGE_THUNK_DATA32));
                    ret.push_back(ci);

                    delete thunkVec;
                    delete origThunkVec;

                    index++;
                    origThunkVec = EXT_INSTANCE.readSessionMemory(base + desc->OriginalFirstThunk + (index * sizeof(IMAGE_THUNK_DATA32)), sizeof(IMAGE_THUNK_DATA32));
                    thunkVec = EXT_INSTANCE.readSessionMemory(base + desc->FirstThunk + (index * sizeof(IMAGE_THUNK_DATA32)), sizeof(IMAGE_THUNK_DATA32));
                    thunk = (PIMAGE_THUNK_DATA32)origThunkVec->data();
                }
                
            }
        }

    cleanup:
        if (nameVec) {
            delete nameVec;
            nameVec = nullptr;
        }

        return ret;
    }
} // namespace wedp