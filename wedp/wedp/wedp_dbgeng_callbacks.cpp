#include "wedp.h"
#include "wedp_dbeng_callbacks.h"


extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

STDMETHODIMP_(ULONG)
WedpDbgEngCallbacks::AddRef()
{
    return InterlockedIncrement(&RefCount);
}


STDMETHODIMP_(ULONG)
WedpDbgEngCallbacks::Release()
{
    if (InterlockedDecrement(&RefCount) == 0)
    {
        delete this;
        return 0;
    }
    return RefCount;
}


STDMETHODIMP
WedpDbgEngCallbacks::GetInterestMask(PULONG Mask)
{
    *Mask = DEBUG_EVENT_LOAD_MODULE | DEBUG_EVENT_UNLOAD_MODULE;
    return S_OK;
}

STDMETHODIMP
WedpDbgEngCallbacks::LoadModule(ULONG64 ImageFileHandle, ULONG64 BaseOffset, ULONG ModuleSize,
    PCSTR ModuleName, PCSTR ImageName, ULONG CheckSum, ULONG TimeDateStamp)
{
    UNREFERENCED_PARAMETER(ImageFileHandle);
    UNREFERENCED_PARAMETER(BaseOffset);
    UNREFERENCED_PARAMETER(ModuleSize);
    UNREFERENCED_PARAMETER(ModuleName);
    UNREFERENCED_PARAMETER(ImageName);
    UNREFERENCED_PARAMETER(CheckSum);
    UNREFERENCED_PARAMETER(TimeDateStamp);

    // Invalidate the Module and Gadget Cache
    // NOTE: EXT_INSTANCE interfaces are not intantiated
    EXT_INSTANCE.h_Module.invalidate_cache();
    EXT_INSTANCE.h_Memory.invalidate_cache();
    EXT_INSTANCE.h_Gadget.invalidate_cache();
    return DEBUG_STATUS_GO_HANDLED;
}


STDMETHODIMP
WedpDbgEngCallbacks::UnloadModule(PCSTR ImageBaseName, ULONG64 BaseOffset)
{
    UNREFERENCED_PARAMETER(ImageBaseName);
    UNREFERENCED_PARAMETER(BaseOffset);

    // Invalidate the Module and Gadget Cache
    // NOTE: EXT_INSTANCE interfaces are not intantiated
    EXT_INSTANCE.h_Memory.invalidate_cache();
    EXT_INSTANCE.h_Gadget.invalidate_cache();
    return DEBUG_STATUS_GO_HANDLED;
}
