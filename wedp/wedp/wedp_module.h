#pragma once
#include "wedp_cache.h"
#include "wedp_options.h"
#include <engextcpp.hpp>
#include <string>
#include <vector>

namespace wedp {

    class WedpModuleImport {
        public:
            std::string moduleName;
            std::string functionName;
            ULONG64 importAddress = 0;
            ULONG64 localAddress = 0;
    };

    // Holds data about a loaded module
    struct WEDP_MODULE {
        // Hold data about a module
        std::string name;
        std::string version;
        std::string path;
        ULONG64     base = 0;
        ULONG64     preferredBase = 0;
        ULONG       size = 0;
        WORD        arch = 0;
        UCHAR       protections = 0;
        std::vector<WedpModuleImport> imports;
    };

    
    // Searched for and caches information about loaded modules in a process
	class WedpModuleHelper : public WedpCache<WEDP_MODULE, ULONG64> {
        // TODO: rethink the access of methods

		public:
            std::string getModulesTable(WedpGenericOptions opts);
            ULONG64 getModuleBase(ULONG64);
            std::string getModulesIat(WedpGenericOptions opts);

            // WedpCache methods
            bool insert(WEDP_MODULE val) override;
            bool exists(ULONG64 val) override;
            const WEDP_MODULE *getvalue(ULONG64 val) override;
            bool remove(ULONG64 val) override;
            std::vector<WEDP_MODULE> filter(WedpGenericOptions opts) override;
            bool checkcachevalidity() override;
            bool updatecache() override; 
        
        private:
            bool parseModule(ULONG64, WEDP_MODULE&);
            std::string getFullModulePath(ULONG64);
            ULONG64 getModulePreferredBase(std::string);
            std::vector<WedpModuleImport> getModuleImports(ULONG64 base, PIMAGE_NT_HEADERS64 ntHdrs, bool x64);

            ULONG cachedPid;
            ULONG cachedPointerLen;         
	};

} // namespace wedp
